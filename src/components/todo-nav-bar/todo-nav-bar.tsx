import {Component} from '@stencil/core';

@Component({
  tag: 'todo-nav-bar',
  styleUrl: 'todo-nav-bar.css',
  shadow: true
})
export class TodoNavBar {

  render() {
    return (
      <div class={'todo-nav-container'}>
        <div class={'todo-nav-inner'}>
          <todo-nav-bar-icon></todo-nav-bar-icon>
          <todo-nav-bar-links></todo-nav-bar-links>
        </div>
      </div>
    );
  }
}
