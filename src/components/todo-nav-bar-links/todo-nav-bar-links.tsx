import {Component, State} from '@stencil/core';

export interface Link {
  name: string;
  link: string;
}

@Component({
  tag: 'todo-nav-bar-links',
  styleUrl: 'todo-nav-bar-links.css',
  shadow: true
})
export class TodoNavBarLinks {
  @State() links: Array<Link> = [
    {name: 'Angular', link: '/todo'},
    {name: 'React', link: '/todoreact'},
    {name: 'HTML', link: '/todohtml'},
    {name: 'Aurelia', link: '/todoaurelia'}
  ];

  render() {
    return (
      <div class={'todo-nav-bar-links-div'}>
        { this.links.map((link) =>
          <todo-nav-bar-link name={link.name} link={link.link}></todo-nav-bar-link>
        )}
      </div>
    );
  }
}
