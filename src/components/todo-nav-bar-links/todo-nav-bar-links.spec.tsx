import { TestWindow } from '@stencil/core/testing';
import { TodoNavBarLinks } from './todo-nav-bar-links';

describe('todo-nav-bar-links', () => {
  it('should build', () => {
    expect(new TodoNavBarLinks()).toBeTruthy();
  });

  describe('rendering', () => {
    let element;
    let window;

    beforeEach(async () => {
      window = new TestWindow();
      element = await window.load({
        components: [TodoNavBarLinks],
        html: '<todo-nav-bar-links></todo-nav-bar-links>'
      });
    });

    it('should work with both the first and the last name', async () => {
      element.first = 'Peter';
      element.last = 'Parker';
      await window.flush();
      expect(element.textContent).toEqual('Hello, my name is Peter Parker');
    });
  });
});
