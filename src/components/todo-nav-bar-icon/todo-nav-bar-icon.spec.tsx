import { TestWindow } from '@stencil/core/testing';
import { TodoNavBarIcon } from './todo-nav-bar-icon';

describe('todo-nav-bar-icon', () => {
  it('should build', () => {
    expect(new TodoNavBarIcon()).toBeTruthy();
  });

  describe('rendering', () => {
    let element;
    let window;

    beforeEach(async () => {
      window = new TestWindow();
      element = await window.load({
        components: [TodoNavBarIcon],
        html: '<todo-nav-bar-icon></todo-nav-bar-icon>'
      });
    });

    it('should work with both the first and the last name', async () => {
      element.first = 'Peter';
      element.last = 'Parker';
      await window.flush();
      expect(element.textContent).toEqual('Hello, my name is Peter Parker');
    });
  });
});
