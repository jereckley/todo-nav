import { Component } from '@stencil/core';

@Component({
  tag: 'todo-nav-bar-icon',
  styleUrl: 'todo-nav-bar-icon.css',
  shadow: true
})
export class TodoNavBarIcon {

  render() {
    return (
      <div class={'to-do-nav-bar-icon-container'}>
        Cross Framework
      </div>
    );
  }
}
