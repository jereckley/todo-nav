import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'todo-nav-bar-link',
  styleUrl: 'todo-nav-bar-link.css',
  shadow: true
})
export class TodoNavBarLink {
  @Prop() name: string;
  @Prop() link: string;

  navigate() {
    window.location.href = this.link
  }

  render() {
    return (
      <div class={'todo-nav-bar-link-div'} onClick={this.navigate.bind(this)}>
        {this.name}
      </div>
    );
  }
}
