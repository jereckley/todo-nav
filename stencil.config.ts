import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'todonavbar',
  outputTargets:[
    {
      type: 'dist'
    },
    {
      type: 'www',
      serviceWorker: null
    }
  ]
};
